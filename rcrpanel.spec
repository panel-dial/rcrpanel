Name:           rcrpanel
Version:        3.5
Release:        4%{?dist}
Summary:        Lay out front panel for electronics project
Group:          Applications/Engineering
License:        GPLv2+
URL:            http://gitorious.org/panel-dial/rcrpanel
Source0:        http://ares-mi.org/qslmaker/downloads/rcrpanel-3.5.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

#BuildRequires: 
#Requires:       

%description
rcrpanel is an application to lay out a front panel for a radio or similar
electronics device.  rcrpanel can provide dials for potentiometers or variable
capacitors as well as lay out cutouts for switches and jacks.  rcrpanel
accepts a panel description file and produces PostScript output.

%prep
%setup -q

%build
#uses make
make rpmdeps

%install
install -D -m 0755 rcrpanel $RPM_BUILD_ROOT%{_bindir}/rcrpanel
install -D -m 0644 rcrpanel.1 $RPM_BUILD_ROOT%{_mandir}/man1/rcrpanel.1

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc rcrpanel.txt Changelog README COPYING AUTHORS
%{_bindir}/rcrpanel
%{_mandir}/man?/rcrpanel*

%changelog
* Wed Apr 13 2011 John McDonough <jjmcd@fedoraproject.org> - 3.5-4
- Update web locations to new server

* Wed Feb 09 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.5-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Tue Nov  2 2010 John McDonough <jjmcd@fedoraproject.org> - 3.5-2
- Remove prelim clean of $RPM_BUILD_ROOT per 529387
- Leaving %clean for now due to build for F12

* Mon Oct 19 2009 John McDonough <jjmcd@fedoraproject.org> - 3.5-1
- Use source from the web
- Update README to eliminate references to Dial
- Include Changelog
- Reflect GPLV2+ in specfile

* Fri Oct 16 2009 John McDonough <jjmcd@fedoraproject.org> - 3.4-2
- Rebuild for F11

* Wed Apr 08 2009 John McDonough <jjmcd@fedoraproject.org> - 3.4-1
- Clean up source to reflect new name
- Check into RCS so rev updated
- Add licensing statement

* Wed Apr 08 2009 John McDonough <jjmcd@fedoraproject.org> - 3.2-1
- First packaging attempt

