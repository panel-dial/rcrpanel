CC = gcc
CFLAGS = $(RPM_OPT_FLAGS)
PANEL_DEPS = rcrpanel.o
TARFILE = rcrpanel-3.4.tar.gz
SRCTAR = rcrpanel-3.4-src.tgz
TAR_DEPS = rcrpanel.c rcrpanel.txt Makefile README rcrpanel.1 \
	AUTHORS COPYING
SRC_DEPS = rcrpanel.c rcrpanel.txt Makefile README rcrpanel.1

.c.o:
	$(CC) -c $(CFLAGS) $<

all: tar srctar

rpmdeps : rcrpanel

rcrpanel : $(PANEL_DEPS)
	$(CC) $< -o $@ -lm

tar : $(TARFILE)

srctar : $(SRCTAR)

$(TARFILE) : $(TAR_DEPS)
	tar -czf $(TARFILE) $^

$(SRCTAR) : $(SRC_DEPS)
	tar -czf $(SRCTAR) $^

rcrpanel.1 : rcrpanel.1.source
	cp rcrpanel.1.source rcrpanel.1

